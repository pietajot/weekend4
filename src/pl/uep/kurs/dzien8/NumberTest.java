package pl.uep.kurs.dzien8;

import static java.lang.Math.E;
import static java.lang.Math.PI;
import static java.lang.Math.sqrt;

import java.math.BigDecimal;
import java.math.BigInteger;

public class NumberTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int x = Integer.parseInt("5");
		Integer y = Integer.valueOf(5);
		float d = Float.parseFloat("0.0");
		Float e = Float.valueOf(0.0f);

		System.out.println("1/3="+1/3);
		System.out.println("1/3="+1/3f);

		float number = -123.45f;

		System.out.println(String.format ("%.1f", number));
		System.out.println(String.format ("%.2f", number));
		System.out.println(String.format ("%.3f", number));
		
		int a1 = 12345;
		int a2 = 12345;
		if (a1 == a2)
			System.out.println("Numbers equal");
		else
			System.out.println("Numbers NOT equal");
		
		Integer b1 = Integer.valueOf(12345);
		Integer b2 = Integer.valueOf(12345);
		if (b1 == b2)
			System.out.println("Numbers equal");
		else
			System.out.println("Numbers NOT equal");

		if (b1.equals(b2))
			System.out.println("Numbers equal");
		else
			System.out.println("Numbers NOT equal");
		
		String s1 = "abc";
		String s2 = "abc";
		
		System.out.println(s1 == s2);
		System.out.println(s1.equals(s2));
		
		if (s1 == s2)
			System.out.println("Strings equal");
		else
			System.out.println("Strings NOT equal");
		
		s1 = "abc";
		s2 = "abcd"; 
		s1 += "d";
		
		System.out.println(s1 == s2);
		System.out.println(s1.equals(s2));
		
		double pierwiastek = sqrt(9);
		
		System.out.println("pierwiastek z 9 = " + pierwiastek);
		
		System.out.println("PI = " + PI);
		System.out.println("E = " + E);
		
		System.out.println(BigInteger.ONE + ", " + BigDecimal.ONE + ", " + BigInteger.ZERO + ", " + BigInteger.TEN);

	}

}
