package pl.uep.kurs.dzien8;

import java.math.*;

public class BigDecimalTest {

   public static void main(String[] args) {
	   Float floatValue = Float.valueOf(1/3f / 12f);
	   Double doubleValue = Double.valueOf(1/3f / 12f);
	   BigDecimal bigValue = BigDecimal.valueOf(1/3f).multiply(BigDecimal.valueOf(1/12f));
	   BigDecimal bigValue2 = BigDecimal.valueOf(1).divide(BigDecimal.valueOf(3), MathContext.DECIMAL128).divide(BigDecimal.valueOf(12), MathContext.DECIMAL128);
	   
	   System.out.println("Float: " + floatValue);
	   System.out.println("Double: " + doubleValue);
	   System.out.println("BigDecimal: " + bigValue);
	   System.out.println("BigDecimal2: " + bigValue2);
	   
	   floatValue = Float.valueOf(0.1f * 0.1f);
	   doubleValue = Double.valueOf(0.1f * 0.1f);
	   bigValue = BigDecimal.valueOf(0.1).multiply(BigDecimal.valueOf(0.1));
	   
	   System.out.println("Float: " + floatValue);
	   System.out.println("Double: " + doubleValue);
	   System.out.println("BigDecimal: " + bigValue);
   }
}