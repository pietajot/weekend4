package pl.uep.kurs.dzien7;

import java.time.LocalDate;

public class Osoba implements IWyswietlanie, IAdresowanie {
	private String imie;
	private String nazwisko;
	private LocalDate dataUrodzenia;
	private Adres adres;
	private String telefon;
	
	public Osoba(String imie, String nazwisko) {
		this.imie = imie;
		this.nazwisko = nazwisko;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public LocalDate getDataUrodzenia() {
		return dataUrodzenia;
	}

	public void setDataUrodzenia(LocalDate dataUrodzenia) {
		this.dataUrodzenia = dataUrodzenia;
	}

	public Adres getAdres() {
		return adres;
	}

	public void setAdres(Adres adres) {
		this.adres = adres;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public void wyswietl() {
		System.out.println(" Imi�: " + getImie() + ", Nazwisko: " + getNazwisko());
	}

}
