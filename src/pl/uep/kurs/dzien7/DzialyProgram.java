package pl.uep.kurs.dzien7;

public class DzialyProgram {

	public static void main(String[] args) {
		Dzial dzial1 = new Dzial(10, "Sprzeda�");
		
		Kierownik kierownik = new Kierownik("Andrzej", "Bartkowiak", "545345435", 5000, 1000);
		kierownik.dodajJezykObcy("angielski");
		kierownik.dodajJezykObcy("niemiecki");
		kierownik.dodajJezykObcy("angielski");
		
		System.out.println("Ile j�zyk�w obcych zna kierownik: " + 
				kierownik.getJezykiObce().size());
		
		kierownik.wyswietl();
		kierownik.wyswietlJezykiObce();
		
		dzial1.setKierownik(kierownik);
		
		kierownik.setTelefon("+48613434343");
			
		Pracownik zastepca = new Pracownik("Micha�", "B�kowski", "67546545", 4500);
		dzial1.setZastepcaKierownika(zastepca);

		zastepca.setTelefon("+48614341253");

		Pracownik pracownik = new Pracownik("Tomasz", "Wi�niewski","5465465465", 3800);
		pracownik.dodajJezykObcy("angielski");
		pracownik.dodajJezykObcy("niemiecki");
		pracownik.dodajJezykObcy("rosyjski");
		pracownik.usunJezykObcy("niemiecki");
		pracownik.wyswietl();
		pracownik.wyswietlJezykiObce();
		
		dzial1.dodajPracownika(pracownik);
		
		pracownik = new Pracownik("Anna", "Michalska","7665465465", 4000);
		dzial1.dodajPracownika(pracownik);
		
		pracownik = new Pracownik("Katarzyna", "Marchlewska", "95030243215", 5000);
		dzial1.dodajPracownika(pracownik);
		
		dzial1.wyswietl();
		
		dzial1.listaPlac();
		
		Pracownik pracPesel = dzial1.getPracownikByPesel("7665465466");
		if ( pracPesel != null ) {
			System.out.println(" Pracownik o podanym nr PESEL istnieje");
			System.out.println(" Jest to : " + pracPesel.getImie() + " " 
										+ pracPesel.getNazwisko());
		}
		else {
			System.out.println(" Pracownik o podanym nr PESEL NIE istnieje!");
		}
	}

}
