package pl.uep.kurs.dzien7;

import java.util.ArrayList;

public class FirmaProgram {

	public static void main(String[] args) {
		Firma firma = utworzFirme();
		
		IBazaKlientow bazaKlientow = utworzBazeKlientow();
		IBazaKlientow bazaKlientowPlik = new BazaKlientowPlik();
		IBazaKlientow bazaKlientowSQL = new BazaKlientowSQL();
		
		firma.setKlienci(bazaKlientow);
		//firma.setKlienci(bazaKlientowPlik);
		//firma.setKlienci(bazaKlientowSQL);
		
		firma.wyswietl();
	}
	
	public static Firma utworzFirme() {
		Dzial dzial = null;
		Pracownik pracownik = null;
		
		Firma firma = new Firma("Firma ABC");
		
		// dzia� "Sprzeda�"
		dzial = new Dzial(10, "Sprzeda�");
		
		pracownik = new Pracownik("Jan", "Kowalski", "80121398432", 5000);
		dzial.dodajPracownika(pracownik);
		
		pracownik = new Pracownik("Tomasz", "Kawalec", "83041398432", 6200);
		dzial.dodajPracownika(pracownik);
		
		pracownik = new Pracownik("Katarzyna", "Marchlewska", "74041398432", 7000);
		dzial.dodajPracownika(pracownik);

		pracownik = new Pracownik("Michalina", "Tomaszewska", "95030198432", 6200);
		dzial.setZastepcaKierownika(pracownik);
		
		pracownik = new Kierownik("Bartosz", "Bosy", "72090209034", 8000, 1500);
		dzial.setKierownik((Kierownik)pracownik);
		
		firma.dodajDzial(dzial);
		
		// dzia� "Marketing"
		dzial = new Dzial(20, "Marketing");
		
		pracownik = new Pracownik("Anna", "Kami�ska", "75121398432", 5500);
		dzial.dodajPracownika(pracownik);
		
		pracownik = new Pracownik("Zbigniew", "Bracki", "65041398432", 7200);
		dzial.dodajPracownika(pracownik);
		
		pracownik = new Pracownik("Barbara", "Musielska", "58031398432", 6900);
		dzial.dodajPracownika(pracownik);
		
		pracownik = new Kierownik("Kazimiera", "Bogacka", "72090209034", 8200, 1700);
		dzial.setKierownik((Kierownik)pracownik);
		
		firma.dodajDzial(dzial);

		// dzia� "Ksiegowo��"
		dzial = new Dzial(30, "Ksi�gowo��");
		
		pracownik = new Pracownik("Joanna", "Z�otowska", "63011398432", 5200);
		dzial.dodajPracownika(pracownik);
		
		pracownik = new Pracownik("Karol", "Szcz�sny", "93011598432", 4600);
		dzial.dodajPracownika(pracownik);
		
		pracownik = new Kierownik("Ewa", "Mucha", "80041609034", 7800, 800);
		dzial.setKierownik((Kierownik)pracownik);
		
		firma.dodajDzial(dzial);

		return firma;
	}
	
	public static IBazaKlientow utworzBazeKlientow() {
		BazaKlientowPamiec baza = new BazaKlientowPamiec();
		
		Klient klient = new Klient("Roman", "Borkowski", "roman@abc.com");
		baza.dodajKlienta(klient);
		
		klient = new Klient("Kazimiera", "Urbaniak", "kazimiera_u@abc.com");
		baza.dodajKlienta(klient);
		
		klient = new Klient("Kazimiera", "Ostatnia", "kazimiera_o@abc.com");
		baza.dodajKlienta(klient);
		
		klient = new Klient("Andrzej", "Barszcz", "andrzej@abc.com");
		baza.dodajKlienta(klient);
		
		klient = new Klient("El�bieta", "Kowalewska", "ela@abc.com");
		baza.dodajKlienta(klient);

		klient = new Klient("Gabriel", "Bukowski", "gabriel@abc.com");
		baza.dodajKlienta(klient);

		return baza;
	}
	
}

